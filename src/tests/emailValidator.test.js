const { expect } = require('chai');
const sinon = require('sinon');
const { validate,validateAsync, validateWithThrow, validateWithLog } = require('../email-validator.js');
 
// describe('first test', () => { 
//   it('should return 2', () => {
//     expect(2).to.equal(2);
//   }) 
// });

describe('validate email', () => {
    it('should return true for valid email addresses', () => {
        const validEmails = [
            'user@outlook.com',
            'john.doe@gmail.com',
            'jane_doe123@yandex.ru'
        ];

        validEmails.forEach(email => {
            expect(validate(email)).to.be.true;
        });
    });

    it('should return false for invalid email addresses', () => {
        const invalidEmails = [
            'user@',
            'user@example',
            '@example.com',
            'john.doe@gmail',
            'jane_doe123'
        ];

        invalidEmails.forEach(email => {
            expect(validate(email)).to.be.false;
        });
    });
});


describe('validateAsync()', () => {
    it('should return true for valid email addresses', async () => {
        const validEmails = [
            'user@outlook.com',
            'john.doe@gmail.com',
            'jane_doe123@yandex.ru'
        ];

        const results = await Promise.all(validEmails.map(email => validateAsync(email)));
        results.forEach(result => {
            expect(result).to.be.true;
        });
    });

    it('should return false for invalid email addresses', async () => {
        const invalidEmails = [
            'user@',
            'user@example',
            '@example.com',
            'john.doe@gmail',
            'jane_doe123'
        ];

        const results = await Promise.all(invalidEmails.map(email => validateAsync(email)));
        results.forEach(result => {
            expect(result).to.be.false;
        });
    });
});


describe('validateWithThrow()', () => {
    it('should return true for valid email addresses', () => {
        const validEmails = [
            'user@outlook.com',
            'john.doe@gmail.com',
            'jane_doe123@yandex.ru'
        ];

        validEmails.forEach(email => {
            expect(validateWithThrow(email)).to.be.true;
        });
    });

    it('should throw an error for invalid email addresses', () => {
        const invalidEmails = [
            'user@',
            'user@example',
            '@example.com',
            'john.doe@gmail',
            'jane_doe123'
        ];

        invalidEmails.forEach(email => {
            expect(() => {
                validateWithThrow(email);
            }).to.throw(`Invalid email: ${email}`);
        });
    });
});

describe('validateWithLog()', () => {
    let consoleLogStub;

    beforeEach(() => {
        // Stub console.log() before each test
        consoleLogStub = sinon.stub(console, 'log');
    });

    afterEach(() => {
        // Restore original console.log() behavior after each test
        consoleLogStub.restore();
    });

    it('should return true for valid email addresses', () => {
        const validEmails = [
            'user@outlook.com',
            'john.doe@gmail.com',
            'jane_doe123@yandex.ru'
        ];

        validEmails.forEach(email => {
            const result = validateWithLog(email);
            expect(result).to.be.true;
        });

        // Verify that console.log() was called
        expect(consoleLogStub.callCount).to.equal(validEmails.length);
    });

    it('should return false for invalid email addresses', () => {
        const invalidEmails = [
            'user@',
            'user@example',
            '@example.com',
            'john.doe@gmail',
            'jane_doe123'
        ];

        invalidEmails.forEach(email => {
            const result = validateWithLog(email);
            expect(result).to.be.false;
        });

        // Verify that console.log() was called
        expect(consoleLogStub.callCount).to.equal(invalidEmails.length);
    });
});






